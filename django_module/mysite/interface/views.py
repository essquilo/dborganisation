from django.http import HttpResponse
from django.template import loader, RequestContext

from database_buissness.database_delete_queries import delete_shop, delete_product, delete_client, delete_sale
from database_buissness.database_insert_queries import put_shop, put_product, put_client, put_sale
from database_buissness.database_select_queries import get_shops, get_clients, get_products, get_sales
from database_buissness.database_update_queries import update_shop, update_product, update_sale, update_client
from database_buissness.file_operations import add_shops, add_products, add_clients, add_sales


tables = [{'text': 'Shops table', 'url': 'shops'}, {'text': 'Clients table', 'url': 'clients'},
          {'text': 'Products table', 'url': 'products'}, {'text': 'Sales table', 'url': 'sales'}]


def index(request):
	template = loader.get_template('index.html')
	context = RequestContext(request, {'table_list': tables}, )
	return HttpResponse(template.render(context))


def show_table(request, table):
	if table == "shops":
		return TableShowers.show_shops(request)
	elif table == "products":
		return TableShowers.show_products(request)
	elif table == "clients":
		return TableShowers.show_clients(request)
	else:
		return TableShowers.show_sales(request)


def start_modifying(request, table, mode):
	if table == "shops":
		return ModifyerStarters.start_modifying_shops(request, table, mode)
	elif table == "products":
		return ModifyerStarters.start_modifying_products(request, table, mode)
	elif table == "clients":
		return ModifyerStarters.start_modifying_clients(request, table, mode)
	elif table == "sales":
		return ModifyerStarters.start_modifying_sales(request, table, mode)


def finish_update(request, table, mode):
	if table == "shops":
		return ModifierFinishers.finish_update_shop(request, table, mode)
	elif table == "products":
		return ModifierFinishers.finish_update_products(request, table, mode)
	elif table == "clients":
		return ModifierFinishers.finish_update_clients(request, table, mode)
	elif table == "sales":
		return ModifierFinishers.finish_update_sales(request, table, mode)


class TableShowers:
	@staticmethod
	def show_shops(request):
		template = loader.get_template('shops_table.html')
		shops = get_shops(request.GET.get("search", None), request.GET.get("filter_names", None))
		context = RequestContext(request, {'shops_list': shops}, )
		return HttpResponse(template.render(context))

	@staticmethod
	def show_clients(request):
		template = loader.get_template('clients_table.html')
		clients = get_clients(request.GET.get("search", None), request.GET.get("filter_names", None), 1 if request.GET.get("filter_legal_entity", None) == "on" else 0)
		context = RequestContext(request, {'clients_list': clients}, )
		return HttpResponse(template.render(context))

	@staticmethod
	def show_products(request):
		template = loader.get_template('products_table.html')
		products = get_products(request.GET.get("search", None), request.GET.get("filter_names", None))
		context = RequestContext(request, {'products_list': products}, )
		return HttpResponse(template.render(context))

	@staticmethod
	def show_sales(request):
		template = loader.get_template('sales_table.html')
		sales = get_sales(request.GET.get("search", None))
		context = RequestContext(request, {'sales_list': sales}, )
		return HttpResponse(template.render(context))


class ModifyerStarters:
	@staticmethod
	def start_modifying_shops(request, table, mode):
		if mode == "add":
			if request.method == 'GET':
				template = loader.get_template('shops_edit.html')
				context = RequestContext(request, {'title': "Add shop"})
			else:
				template = loader.get_template('update_success.html')
				if add_shops(request.FILES['file']):
					context = RequestContext(request, {'title': "   Adding successful<br>   Redirecting...", 'table': table}, )
				else:
					context = RequestContext(request, {'title': "   Error adding from file<br>   Redirecting...", 'table': table}, )
			return HttpResponse(template.render(context))
		elif mode == "update":
			template = loader.get_template('shops_edit.html')
			context = RequestContext(request, {'title': "Update shop", 'old_id': request.GET.get('id', "Null"),
			                                   'name': request.GET.get('name', "Null"),
			                                   'address': request.GET.get('address', "Null")})
			return HttpResponse(template.render(context))
		elif mode == "delete":
			return Deleters.delete_shop(request, table)

	@staticmethod
	def start_modifying_products(request, table, mode):
		if mode == "add":
			if request.method=="GET":
				template = loader.get_template('products_edit.html')
				context = RequestContext(request, {'title': "Add product"}, )
			else:
				template = loader.get_template('update_success.html')
				if add_products(request.FILES['file']):
					context = RequestContext(request, {'title': "   Adding successful<br>   Redirecting...", 'table': table}, )
				else:
					context = RequestContext(request, {'title': "   Error adding from file<br>   Redirecting...", 'table': table}, )
			return HttpResponse(template.render(context))
		elif mode == "update":
			template = loader.get_template('products_edit.html')
			context = RequestContext(request, {'title': "Update product", 'old_id': request.GET.get('id', "Null"),
			                                   'name': request.GET.get('name', "Null"),
			                                   'price': request.GET.get('price', "Null")})
			return HttpResponse(template.render(context))
		elif mode == "delete":
			return Deleters.delete_product(request, table)

	@staticmethod
	def start_modifying_clients(request, table, mode):
		if mode == "add":
			if request.method == "GET":
				template = loader.get_template('clients_edit.html')
				context = RequestContext(request, {'title': "Add client"}, )
			else:
				template = loader.get_template('update_success.html')
				if add_clients(request.FILES['file']):
					context = RequestContext(request, {'title': "   Adding successful<br>   Redirecting...", 'table': table}, )
				else:
					context = RequestContext(request, {'title': "   Error adding from file<br>   Redirecting...", 'table': table}, )
			return HttpResponse(template.render(context))
		elif mode == "update":
			template = loader.get_template('clients_edit.html')
			context = RequestContext(request, {'title': "Update client", 'legal_entity': request.GET.get('legal_entity', "Null"),
			                                   'name': request.GET.get('name', "Null"),
			                                   'age': request.GET.get('age', "Null")})
			return HttpResponse(template.render(context))
		elif mode == "delete":
			return Deleters.delete_client(request, table)

	@staticmethod
	def start_modifying_sales(request, table, mode):
		products = get_products(None)
		shops = get_shops(None, None)
		clients = get_clients(None)
		if mode == "add":
			if request.method == "GET":
				template = loader.get_template('sales_edit.html')
				context = RequestContext(request, {'title': "Add sale", 'products': products, 'clients': clients, 'shops': shops}, )
			else:
				template = loader.get_template('update_success.html')
				if add_sales(request.FILES['file']):
					context = RequestContext(request, {'title': "   Adding successful<br>   Redirecting...", 'table': table}, )
				else:
					context = RequestContext(request, {'title': "   Error adding from file<br>   Redirecting...", 'table': table}, )
			return HttpResponse(template.render(context))
		elif mode == "update":
			template = loader.get_template('sales_edit.html')
			context = RequestContext(request, {'title': "Update sale", 'product_id': request.GET.get('product_id', "Null"),
			                                   'client_name': request.GET.get('client_name', "Null"),
			                                   'shop_id': request.GET.get('shop_id', "Null"), 'item_numbers': request.GET.get('number', 0),
			                                   'products': products, 'clients': clients, 'shops': shops})
			return HttpResponse(template.render(context))
		elif mode == "delete":
			return Deleters.delete_client(request, table)


class ModifierFinishers:
	@staticmethod
	def finish_update_shop(request, table, mode):
		global response
		template = loader.get_template('update_success.html')
		if mode == "add":
			put_shop(request.POST.get("new_id", 0), request.POST.get("name", ""), request.POST.get("address", ""))
			context = RequestContext(request, {'title': "   Adding successful<br>   Redirecting...", 'table': table}, )
		elif mode == "update":
			update_shop(request.POST.get("old_id", 0), request.POST.get("new_id", 0), request.POST.get("name", ""),
			            request.POST.get("address", ""))
			context = RequestContext(request, {'title': "   Update successful<br>   Redirecting...", 'table': table}, )
		else:
			context = RequestContext(request, {'title': "   Wrong request<br>   Redirecting...", 'table': table}, )
		response = HttpResponse(template.render(context))
		return response

	@staticmethod
	def finish_update_products(request, table, mode):
		global response
		template = loader.get_template('update_success.html')
		if mode == "add":
			put_product(request.POST.get("new_id", 0), request.POST.get("name", ""), request.POST.get("price", 0))
			context = RequestContext(request, {'title': "   Adding successful<br>   Redirecting...", 'table': table}, )
		elif mode == "update":
			update_product(request.POST.get("old_id", 0), request.POST.get("new_id", 0), request.POST.get("name", ""),
			               request.POST.get("price", 0))
			context = RequestContext(request, {'title': "   Update successful<br>   Redirecting...", 'table': table}, )
		else:
			context = RequestContext(request, {'title': "   Wrong request<br>   Redirecting...", 'table': table}, )
		response = HttpResponse(template.render(context))
		return response

	@staticmethod
	def finish_update_clients(request, table, mode):
		global response
		template = loader.get_template('update_success.html')
		if mode == "add":
			if request.POST.get("new_name", "") != "":
				put_client(request.POST.get("new_name", ""), request.POST.get("age", 0), 1 if request.POST.get("legal_entity", "off") == "on" else 0)
				context = RequestContext(request, {'title': "   Adding successful<br>   Redirecting...", 'table': table}, )
			else:
				context = RequestContext(request, {'title': "   Adding error<br>   Redirecting...", 'table': table}, )
		elif mode == "update":
			update_client(request.POST.get("old_name", ""), request.POST.get("new_name", ""),
			               request.POST.get("age", "0"), 1 if request.POST.get("legal_entity", "off") == "on" else 0)
			context = RequestContext(request, {'title': "   Update successful<br>   Redirecting...", 'table': table}, )
		else:
			context = RequestContext(request, {'title': "   Wrong request<br>   Redirecting...", 'table': table}, )
		response = HttpResponse(template.render(context))
		return response

	@staticmethod
	def finish_update_sales(request, table, mode):
		global response
		template = loader.get_template('update_success.html')
		if mode == "add":
			put_sale(request.POST.get("item_numbers", 0), request.POST.get("new_product_id", 0), request.POST.get("new_shop_id", 0),
			request.POST.get("new_client_name", ""))
			context = RequestContext(request, {'title': "   Adding successful<br>   Redirecting...", 'table': table}, )
		elif mode == "update":
			update_sale(request.POST.get("new_product_id", 0), request.POST.get("new_shop_id", 0),
				         request.POST.get("new_client_name", ""), request.POST.get("item_numbers", 0),
				         request.POST.get("old_product_id", 0), request.POST.get("old_shop_id", 0),
				         request.POST.get("old_client_name", ""))
			context = RequestContext(request, {'title': "   Update successful<br>   Redirecting...", 'table': table}, )
		else:
			context = RequestContext(request, {'title': "   Wrong request<br>   Redirecting...", 'table': table}, )
		response = HttpResponse(template.render(context))
		return response


class Deleters:
	@staticmethod
	def delete_shop(request, table):
		template = loader.get_template('update_success.html')
		id = request.GET.get("id", 0)
		if delete_shop(id) is not None:
			context = RequestContext(request, {'title': "   Delete successful<br>   Redirecting...", 'table': table}, )
		else:
			context = RequestContext(request, {'title': "   Delete error<br>   Redirecting...", 'table': table}, )
		response = HttpResponse(template.render(context))
		return response

	@staticmethod
	def delete_product(request, table):
		template = loader.get_template('update_success.html')
		id = request.GET.get("id", 0)
		if delete_product(id) is not None:
			context = RequestContext(request, {'title': "   Delete successful<br>   Redirecting...", 'table': table}, )
		else:
			context = RequestContext(request, {'title': "   Delete error<br>   Redirecting...", 'table': table}, )
		response = HttpResponse(template.render(context))
		return response

	@staticmethod
	def delete_client(request, table):
		template = loader.get_template('update_success.html')
		name = request.GET.get("name, """)
		if delete_client(name) is not None:
			context = RequestContext(request, {'title': "   Delete successful<br>   Redirecting...", 'table': table}, )
		else:
			context = RequestContext(request, {'title': "   Delete error<br>   Redirecting...", 'table': table}, )
		response = HttpResponse(template.render(context))
		return response

	@staticmethod
	def delete_sale(request, table):
		template = loader.get_template('update_success.html')
		product_id = request.GET.get("product_id", 0)
		client_name = request.GET.get("client_name", "")
		shop_id = request.GET.get("shop_id", 0)
		if delete_sale(shop_id, client_name, product_id) is not None:
			context = RequestContext(request, {'title': "   Delete successful<br>   Redirecting...", 'table': table}, )
		else:
			context = RequestContext(request, {'title': "   Delete error<br>   Redirecting...", 'table': table}, )
		response = HttpResponse(template.render(context))
		return response