from django.conf.urls import patterns, url

from django_module.mysite.interface import views


urlpatterns = patterns('',
                       url(r'^$', views.index, name='index'),
                       url(r'(?P<table>\w*)/(?P<mode>\w*)/finish$', views.finish_update, name='finish_update'),
                       url(r'(?P<table>\w*)/(?P<mode>\w*)/$', views.start_modifying, name='start_modifying'),
                       url(r'(?P<table>\w*)/$', views.show_table, name='show_table'),


)
