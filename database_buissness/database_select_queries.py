from string import strip

__author__ = 'Esquilo'
import sys

import MySQLdb as Mdb


clients_table = 'client_table'
product_table = 'product_table'
sales_table = 'sales_table'
shops_table = 'shop_table'


def get_shops(search_query, names):
	global con
	try:
		con = Mdb.connect('localhost', 'root', '9999', 'lab1_schema')
		query = 'SELECT * FROM ' + shops_table
		if search_query:
			query += " WHERE NOT MATCH (name, address) AGAINST (\'" + search_query + "\')"
		elif names:
			query += " WHERE " + form_selection_query("name", names.split(","))
		cur = con.cursor(Mdb.cursors.DictCursor)
		cur.execute(query)
		return cur.fetchall()
	except Mdb.Error, e:
		print "Error %d: %s" % (e.args[0], e.args[1])
		sys.exit(1)
	finally:
		if con:
			con.close()


def get_clients(search_query, names, legal_entity):
	global con
	try:
		con = Mdb.connect('localhost', 'root', '9999', 'lab1_schema')
		query = 'SELECT * FROM ' + clients_table
		if search_query:
			query += " WHERE NOT MATCH (name) AGAINST (\'" + search_query + "\')"
		elif names:
			query += " WHERE " + form_selection_query("name", names.split(","))
		elif legal_entity:
			query += " WHERE legal_entity='" + str(legal_entity) + "'"
		cur = con.cursor(Mdb.cursors.DictCursor)
		cur.execute(query)
		return cur.fetchall()
	except Mdb.Error, e:
		print "Error %d: %s" % (e.args[0], e.args[1])
		sys.exit(1)
	finally:
		if con:
			con.close()


def get_products(search_query, names):
	global con
	try:
		con = Mdb.connect('localhost', 'root', '9999', 'lab1_schema')
		query = 'SELECT * FROM ' + product_table
		if search_query:
			query += " WHERE NOT MATCH (name) AGAINST (\'" + search_query + "\')"
		elif names:
			query += " WHERE " + form_selection_query("name", names.split(","))
		cur = con.cursor(Mdb.cursors.DictCursor)
		cur.execute(query)
		return cur.fetchall()
	except Mdb.Error, e:
		print "Error %d: %s" % (e.args[0], e.args[1])
		sys.exit(1)
	finally:
		if con:
			con.close()


def get_sales(search_query):
	global con
	try:
		con = Mdb.connect('localhost', 'root', '9999', 'lab1_schema')
		query = 'SELECT sales_table.item_numbers AS item_numbers, product_table.id AS product_id,' \
		        ' product_table.name AS product_name' \
		        ', product_table.price AS product_price,' \
		        ' client_table.name AS client_name, client_table.age AS client_age,' \
		        ' client_table.legal_entity AS legal_entity, shop_table.id AS shop_id,' \
		        ' shop_table.name AS shop_name, shop_table.address AS shop_address' \
		        ' FROM sales_table INNER JOIN product_table ON sales_table.product_table_id=product_table.id' \
		        ' INNER JOIN shop_table ON sales_table.shop_table_id=shop_table.id' \
		        ' INNER JOIN client_table ON sales_table.client_table_name=client_table.name'
		if search_query:
			query += " WHERE NOT MATCH (client_table.name, product_table.name, shop_table.name, shop_table.address) AGAINST (\'" + search_query + "\')"
		cur = con.cursor(Mdb.cursors.DictCursor)
		cur.execute(query)
		return cur.fetchall()
	except Mdb.Error, e:
		print "Error %d: %s" % (e.args[0], e.args[1])
		sys.exit(1)
	finally:
		if con:
			con.close()


def form_selection_query(selection, args):
	return reduce(lambda res, x: res + " " + selection +"='" + x.strip(" ")+ "' OR ",args, "").strip(" ,OR")