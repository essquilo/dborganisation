__author__ = 'Esquilo'
import MySQLdb as Mdb
import sys

clients_table = 'client_table'
product_table = 'product_table'
sales_table = 'sales_table'
shops_table = 'shop_table'


def put_shop(id, name, address):
	global con
	try:
		con = Mdb.connect('localhost', 'root', '9999', 'lab1_schema')
		query = 'INSERT INTO `lab1_schema`.`shop_table`' \
		        ' (`id`, `name`, `address`) ' \
		        'VALUES (\'' + str(id) + '\', \'' + name + '\', \'' + address + '\')'
		cur = con.cursor()
		cur.execute(query)
		return con.commit()
	except Mdb.Error, e:
		print "Error %d: %s" % (e.args[0], e.args[1])
		return None
	finally:
		if con:
			con.close()


def put_client(name, age, legal_entity):
	global con
	try:
		con = Mdb.connect('localhost', 'root', '9999', 'lab1_schema')
		query = 'INSERT INTO `lab1_schema`.`client_table` ' \
		        '(`name`, `age`, `legal_entity`)' \
		        ' VALUES (\'' + name + '\', \'' + str(age) + '\', \'' + ('1' if legal_entity else '0') + '\')'
		print query
		cur = con.cursor()
		cur.execute(query)
		return con.commit()
	except Mdb.Error, e:
		print "Error %d: %s" % (e.args[0], e.args[1])
		return None
	finally:
		if con:
			con.close()


def put_product(id, name, price):
	global con
	try:
		con = Mdb.connect('localhost', 'root', '9999', 'lab1_schema')
		query = 'INSERT INTO `lab1_schema`.`product_table`' \
		        '(`id`, `name`, `price`)' \
		        'VALUES (\'' + str(id) + '\', \'' + name + '\', \'' + str(price) + '\')'
		cur = con.cursor()
		cur.execute(query)
		return con.commit()
	except Mdb.Error, e:
		print "Error %d: %s" % (e.args[0], e.args[1])
		return None
	finally:
		if con:
			con.close()


def put_sale(item_numbers, product_table_id, shop_table_id, client_table_name):
	global con
	try:
		con = Mdb.connect('localhost', 'root', '9999', 'lab1_schema')
		query = 'INSERT INTO `lab1_schema`.`sales_table` ' \
		        '(`item_numbers`, `product_table_id`, `shop_table_id`, `client_table_name`)' \
		        ' VALUES (\'' + str(item_numbers) + '\', \'' + str(product_table_id) + '\', \'' + str(
			shop_table_id) + '\', \'' + client_table_name + '\');'
		cur = con.cursor()
		cur.execute(query)
		return con.commit()
	except Mdb.Error, e:
		print "Error %d: %s" % (e.args[0], e.args[1])
		return None
	finally:
		if con:
			con.close()