__author__ = 'Esquilo'
import sys

import MySQLdb as Mdb


def update_shop(old_id, new_id, name, address):
	global con
	try:
		con = Mdb.connect('localhost', 'root', '9999', 'lab1_schema')
		query = 'UPDATE `lab1_schema`.`shop_table`' \
		        ' SET `id`=\'' + str(new_id) + '\', `name`= \'' + name + '\', `address`=\'' + address + '\'' \
		                                                                                                ' WHERE  `id`=\'' + str(
			old_id) + '\''
		cur = con.cursor()
		with con:
			cur.execute(query)
			con.commit()
	except Mdb.Error, e:
		print "Error %d: %s" % (e.args[0], e.args[1])
		sys.exit(1)
	finally:
		if con:
			con.close()


def update_client(old_name, new_name, age, legal_entity):
	global con
	try:
		con = Mdb.connect('localhost', 'root', '9999', 'lab1_schema')
		query = 'UPDATE `lab1_schema`.`client_table` ' \
		        ' SET `name`=\'' + new_name + '\', `age`=\'' + str(age) + '\', ' \
		        '`legal_entity`=\'' + ('1' if legal_entity else '0') + '\'' \
		        'WHERE `name`=\'' + old_name + '\''
		print query
		cur = con.cursor()
		with con:
			cur.execute(query)
			con.commit()
	except Mdb.Error, e:
		print "Error %d: %s" % (e.args[0], e.args[1])
		sys.exit(1)
	finally:
		if con:
			con.close()


def update_product(old_id, new_id, name, price):
	global con
	try:
		con = Mdb.connect('localhost', 'root', '9999', 'lab1_schema')
		query = 'UPDATE `lab1_schema`.`product_table`' \
		        ' SET `id`=\'' + str(new_id) + '\', `name`=\'' + name + '\', `price`=\'' + str(price) + '\'' \
		                                                                                                ' WHERE `id`=\'' + str(
			old_id) + '\''
		cur = con.cursor()
		with con:
			cur.execute(query)
			con.commit()
	except Mdb.Error, e:
		print "Error %d: %s" % (e.args[0], e.args[1])
		sys.exit(1)
	finally:
		if con:
			con.close()


def update_sale(old_product_table_id, old_shop_table_id, old_client_table_name, item_numbers, new_product_table_id, new_shop_table_id,
                new_client_table_name):
	global con
	try:
		con = Mdb.connect('localhost', 'root', '9999', 'lab1_schema')
		query = 'UPDATE `lab1_schema`.`sales_table` ' \
		        ' SET `item_numbers`=\'' + str(item_numbers) + '\', `product_table_id`=\'' + str(new_product_table_id) + '\', ' \
		                                                                                                                 '`shop_table_id`=\'' + str(
			new_shop_table_id) + '\', `client_table_name`= \'' + new_client_table_name + '\'' \
		                                                                                 ' WHERE `product_table_id`=\'' + str(
			old_product_table_id) + '\' AND ' \
		                            ' `shop_table_id`=\'' + str(old_shop_table_id) + '\' AND `client_table_name`=\'' + str(
			old_client_table_name) + '\''
		cur = con.cursor()
		with con:
			cur.execute(query)
			con.commit()
	except Mdb.Error, e:
		print "Error %d: %s" % (e.args[0], e.args[1])
		sys.exit(1)
	finally:
		if con:
			con.close()