__author__ = 'Esquilo'
import MySQLdb as Mdb
import sys

clients_table = 'client_table'
product_table = 'product_table'
sales_table = 'sales_table'
shops_table = 'shop_table'


def delete_shop(id):
	global con
	try:
		con = Mdb.connect('localhost', 'root', '9999', 'lab1_schema')
		query = 'DELETE  FROM ' + shops_table + " WHERE id=" + str(id)
		with con:
			cur = con.cursor(Mdb.cursors.DictCursor)
			cur.execute(query)
			return cur.fetchall()
	except Mdb.Error, e:
		print "Error %d: %s" % (e.args[0], e.args[1])
		return None
	finally:
		if con:
			con.close()


def delete_client(name):
	global con
	try:
		con = Mdb.connect('localhost', 'root', '9999', 'lab1_schema')
		query = 'DELETE  FROM ' + clients_table + " WHERE id=" + name
		with con:
			cur = con.cursor(Mdb.cursors.DictCursor)
			cur.execute(query)
			return cur.fetchall()
	except Mdb.Error, e:
		print "Error %d: %s" % (e.args[0], e.args[1])
		return None

	finally:
		if con:
			con.close()


def delete_product(id):
	global con
	try:
		con = Mdb.connect('localhost', 'root', '9999', 'lab1_schema')
		query = query = 'DELETE  FROM ' + product_table + " WHERE id=" + str(id)
		with con:
			cur = con.cursor(Mdb.cursors.DictCursor)
			cur.execute(query)
			return cur.fetchall()
	except Mdb.Error, e:
		print "Error %d: %s" % (e.args[0], e.args[1])
		return None
	finally:
		if con:
			con.close()


def delete_sale(shop_id, client_name, product_id):
	global con
	try:
		con = Mdb.connect('localhost', 'root', '9999', 'lab1_schema')
		query = 'DELETE  FROM ' + sales_table + " WHERE product_table_ID=" + str(product_id) + ", shop_table_id=" \
		        + str(shop_id) + ", " \
				"client_table_name=" + client_name
		with con:
			cur = con.cursor(Mdb.cursors.DictCursor)
			cur.execute(query)
			return cur.fetchall()
	except Mdb.Error, e:
		print "Error %d: %s" % (e.args[0], e.args[1])
		return None
	finally:
		if con:
			con.close()