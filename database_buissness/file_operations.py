from database_buissness.database_insert_queries import put_shop, put_product, put_client, put_sale

__author__ = 'Esquilo'
import json


def add_shops(file):
	json_data = file.read()
	try:
		shops = json.loads(json_data)
		for shop in shops:
			if not put_shop(shop['id'], shop['name'], shop['address']):
				return None
		return 1
	except Exception, e:
		print e
		return None


def add_products(file):
	json_data = file.read()
	try:
		products = json.loads(json_data)
		for product in products:
			if not put_product(product['id'], product['name'], product['price']):
				return None
		return 1
	except Exception, e:
		print e
		return None


def add_clients(file):
	json_data = file.read()
	try:
		clients = json.loads(json_data)
		for client in clients:
			if not put_client(client['name'], client['age'], client['legal_entity']):
				return None
		return 1
	except Exception, e:
		print e
		return None


def add_sales(file):
	json_data = file.read()
	try:
		sales = json.loads(json_data)
		for sale in sales:
			if not put_sale(sale['item_numbers'], sale['product_table_id'], sale['shop_table_id'], sale['client_table_name']):
				return None
		return 1
	except Exception, e:
		print e
		return None